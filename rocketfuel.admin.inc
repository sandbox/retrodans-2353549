<?php

/**
 * @file
 * Admin callbacks for the RocketFuel module.
 */

/**
 * Implements hook_admin_settings().
 */
function rocketfuel_admin_settings_form() {

  // General settings.
  $form['account'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('General settings'),
  );
  $form['account']['rocketfuel_account'] = array(
    '#type'           => 'textfield',
    '#title'          => t('RocketFuel AccountID (rb=)'),
    '#default_value'  => variable_get('rocketfuel_account', '3'),
    '#required'       => TRUE,
    '#description'    => t('The account supplied to you by RocketFuel.  For more information go to <a href="@rf_url">@rf_url</a>', array('@rf_url' => 'http://www.rocketfuel.com')),
  );
  $form['account']['rocketfuel_action'] = array(
    '#type'           => 'textfield',
    '#title'          => t('RocketFuel Default ActionID (ca=)'),
    '#default_value'  => variable_get('rocketfuel_action', ''),
    '#description'    => t('The actionID supplied to you by RocketFuel, you will likely want to leave this blank, and if required, use advanced for page specific actions'),
  );
  $form['account']['rocketfuel_domain'] = array(
    '#type'           => 'textfield',
    '#title'          => t('RocketFuel Domain'),
    '#default_value'  => variable_get('rocketfuel_domain', '1991p.rfihub.com'),
    '#required'       => TRUE,
    '#description'    => t('The domain used in the rocketfuel code, the docs use: 1991p.rfihub.com'),
  );

  // Advanced settings.
  $form['advanced'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('Advanced settings'),
    '#collapsible'    => TRUE,
    '#collapsed'      => TRUE,
  );
  $form['advanced']['rocketfuel_js_scope'] = array(
    '#type'           => 'select',
    '#title'          => t('Javascript Scope'),
    '#description'    => t('RocketFuel recommends adding its Javascript code to the footer, just before the &lt;/body&gt; tag and any other measurement tags. At present, this is the only method recommended by this module.'),
    '#options'        => array(
      'page_bottom'     => t('Footer'),
    ),
    '#default_value'  => variable_get('rocketfuel_js_scope', 'page_bottom'),
  );
  $form['advanced']['rocketfuel_embed_type'] = array(
    '#type'           => 'select',
    '#title'          => t('Embed Type'),
    '#description'    => t('Rocketfuel offers 2 types of embed, I default this to the standard js method, although you can switch to the plain image method if required.'),
    '#options'        => array(
      'js'     => t('Javascript'),
      'image'     => t('Image'),
    ),
    '#default_value'  => variable_get('rocketfuel_embed_type', 'js'),
  );
  $form['advanced']['rocketfuel_secure'] = array(
    '#type'           => 'radios',
    '#title'          => t('Security'),
    '#description'    => t('Does your site run under http or https'),
    '#options'        => array(
      'http'     => t('HTTP'),
      'https'     => t('HTTPS'),
    ),
    '#default_value'  => variable_get('rocketfuel_secure', 'http'),
  );
  $form['advanced']['rocketfuel_action_overrides'] = array(
    '#type'           => 'textarea',
    '#title'          => t('Action page specific overrides'),
    '#description'    => t('The action overrides to use on the site for page specifics, these will then replace the default set above (if used).  Use in the format &lt;path&gt;|&lt;id&gt;'),
    '#default_value'  => variable_get('rocketfuel_action_overrides', ''),
  );

  $form['advanced']['rocketfuel_rules'] = array(
    '#type'           => 'textarea',
    '#title'          => t('Rules'),
    '#description'    => t('What paths can/cannot be used to render RocketFuel stats variables on.  Sometimes we only want it on specific pages, this will allow that to happen.  You will need to set this at least to the pages listed above in the overrides if you change from an *'),
    '#default_value'  => variable_get('rocketfuel_rules', '*'),
  );

  return system_settings_form($form);

}
