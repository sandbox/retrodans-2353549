<?php
/**
 * @file
 * Render the shorter image tracking code for RocketFuel.
 *
 * Available variables:
 * - $vars: An array of variables passed in from the page via the module
 */
?>
<!-- Begin Rocket Fuel Conversion Action Tracking Code Version 7 -->
<img src='<?php print $vars['security'] ?>://<?php print $vars['domain'] ?>/ca.gif?rb=<?php print $vars['account'] ?><?php print (!empty($vars['actions']) ? "&ca=" . $vars['actions'] : "") ?>&ra=' height=0 width=0 style='display:none' alt='Rocket Fuel'/>
<!-- End Rocket Fuel Conversion Action Tracking Code Version 7 -->
