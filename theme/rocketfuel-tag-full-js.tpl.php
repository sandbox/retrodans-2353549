<?php
/**
 * @file
 * Render the full js onto the page for RocketFuel.
 *
 * Available variables:
 * - $vars: An array of variables passed in from the page via the module
 */
?>
<!-- Begin Rocket Fuel Conversion Action Tracking Code Version 7 -->
<script type="text/javascript">
  var cache_buster = parseInt(Math.random()*99999999);
  document.write("<img src='<?php print $vars['security'] ?>://<?php print $vars['domain'] ?>/ca.gif?rb=<?php print $vars['account'] ?><?php print (!empty($vars['actions']) ? "&ca=" . $vars['actions'] : "") ?>&ra=" + cache_buster + "' height=0 width=0 style='display:none' alt='Rocket Fuel'/>");
</script>
<noscript>
  <iframe src='<?php print $vars['security'] ?>://<?php print $vars['domain'] ?>/ca.html?rb=<?php print $vars['account'] ?><?php print (!empty($vars['actions']) ? "&ca=" . $vars['actions'] : "") ?>&ra=' style='display:none;padding:0;margin:0' width='0' height='0'></iframe>
</noscript>
<!-- End Rocket Fuel Conversion Action Tracking Code Version 7 -->
