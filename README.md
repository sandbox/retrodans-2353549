A module to integrate Drupal with the RocketFuel analytics system 
(http://rocketfuel.com/)

== What is RocketFuel?
"Rocket Fuel delivers a leading programmatic media-buying platform at Big Data 
scale that harnesses the power of artificial intelligence to improve marketing 
ROI in digital media across web, mobile, video, and social channels. Rocket 
Fuel powers digital advertising and marketing programs globally for customers 
in North America, Europe, and Japan." - http://rocketfuel.com/

== Features
* Simple setup of RocketFuel tracking
* Advanced configuration for setting Action on specific pages, or disabling 
tracking by URL
* Options for choosing javascript or image tracking
* Choice between the http or https code

== Installation and basic configuration
* Put module into the usual place (sites/all/modules/contrib/)
* Enable module on /admin/modules
* Configure permissions on /admin/people/permissions
* Paste in your AccountID on /admin/config/system/rocketfuel

== Further configuration (/admin/config/system/rocketfuel)
* Set a sitewide action ID
* Change the type of tracking code
* Set different ActionIDs for specific pages
* Set the pages which RocketFuel tracking should appear on (defaults to all)

== Thankyous
* The QuantCast module, which I used as a template to speed up development on 
this module
* A patch I had originally written for the QuantCast module to help extend 
it's flexibility around labels, and only inserting onto specific pages
